FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY build/libs/config-0.0.1-SNAPSHOT.jar config-service.jar
ENTRYPOINT ["java", "-jar", "config-service.jar"]