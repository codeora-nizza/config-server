When we use the local file system as the configuration repository, 
we should use the native profile

```$xslt
./gradlew bootRun -Dspring.profiles.active=native
```

This native profile is defined by the Spring Cloud Config.
See doc [2.1.3 File System Backend](https://cloud.spring.io/spring-cloud-config/multi/multi__spring_cloud_config_server.html)

To see the native profile configuration
```$xslt
curl localhost:8888/config-server/native
```

Run docker 
```$xslt
$ docker build .
...
$ docker image ls
...
$ docker container run -p 8888:8888 -it <image id>
```
To run it in detached mode
```aidl
$ docker container run -d -p 8888:8888 --name config-server nizza-config-service:latest
...
$ docker inspect config-server
...
"Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "babff9973f881f2f97d89342072ebca89535bb746378add7dc60aba464f96dbd",
                    "EndpointID": "789655a3d76b167aa6f4f32cd74bddeb5eb5d01be228415702025d352a41de23",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:02",
                    "DriverOpts": null
                }
            }
... 

```

Run docker in a specified network and with a specific name

```aidl
$ docker container run ... --network=<network name> ... 
```

For applications running in different containers to communicate with each other,
the containers must be in the same network.  

Using docker compose will automatically put services in the same 
network